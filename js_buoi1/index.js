// start lab 1
/*
- Input:
    + Tiền lương 1 ngày
    + Số ngày làm
- Process:
    + Tạo 2 biến cho tiền lương 1 ngày và số ngày làm
    + Tạo biến tính tiền lương nhân viên
    + Gán giá trị cho 2 biến tiền lương 1 ngày và số ngày làm
    + Tính tiền lương nhân viên
    + In ra màn hình tiền lương nhân viên
- Output:
    + Tiền lương
 */

function tinhTongLuong() {
  var tienLuong1ds =
    parseFloat(document.getElementById("tienLuong1d").innerHTML) * 1000;
  var soNgayLams = parseFloat(document.getElementById("soNgayLam").value);

  var tinhTongLuongs = 0;
  tinhTongLuongs = tienLuong1ds * soNgayLams;

  document.getElementById("tongLuong").innerHTML =
    tinhTongLuongs.toLocaleString();
}
// start lab 2
/*
- Input:
    + Số thực
- Process:
    + Tạo lần lượt 5 biến số thực
    + Tạo biến tính giá trị trung bình
    + Gán lần lượt giá trị cho 5 biến số thực
    + Tính giá trị trung bình 5 sô thực
    + In ra màn hình giá trị trung bình
- Output:
    + Giá trị trung bình bình
 */
function tinhTrungBinh() {
  var num1 = parseFloat(document.getElementById("soThuc1").value);
  var num2 = parseFloat(document.getElementById("soThuc2").value);
  var num3 = parseFloat(document.getElementById("soThuc3").value);
  var num4 = parseFloat(document.getElementById("soThuc4").value);
  var num5 = parseFloat(document.getElementById("soThuc5").value);

  var giaTriTrungBinhs = 0;
  giaTriTrungBinhs = (num1 + num2 + num3 + num4 + num5) / 5;

  document.getElementById("giaTriTrungBinh").innerHTML =
    giaTriTrungBinhs.toLocaleString();
}
// start lab 3
/*
- Input:
    + Giá usd hiện tại
    + số usd nhập vào
- Process:
    + Tạo biến giá usd hiện tại và số usd nhập vào
    + Tạo biến kết quả quy đổi tiền
    + Gán giá trị cho biến giá usd hiện tại và số usd nhập vào
    + Tính giá trị quy đổi tiền
    + In ra màn hình kết quả quy đổi tiền
- Output:
    + Kêt quả quy đổi tiền
 */
function quyDoiTien() {
  var giaUSDs = parseFloat(document.getElementById("giaUSD").innerHTML) * 1000;
  var soUSDs = parseFloat(document.getElementById("soUSD").value);

  var quyDois = 0;
  quyDois = giaUSDs * soUSDs;

  document.getElementById("quyDoi").innerHTML = quyDois.toLocaleString();
}
// start lab 4
/*
- Input:
    + Chiều dài, Chiều rộng
- Process:
    + Tạo biến chiều dài, chiều rộng
    + Tạo biến diện tích, chu vi
    + Gán giá trị cho biến chiều dài, chiều rộng
    + Tính diện tích, chu vi
    + In ra màn hình diện tích và chu vi
- Output:
    + Diện tích, chu vi
 */
function tinhDTCV() {
  var chieuDais = parseFloat(document.getElementById("chieuDai").value);
  var chieuRongs = parseFloat(document.getElementById("chieuRong").value);

  var dienTichs = 0;
  dienTichs = chieuDais * chieuRongs;
  var chuVis = 0;
  chuVis = (chieuDais + chieuRongs) * 2;

  document.getElementById("dienTich").innerHTML = dienTichs.toLocaleString();
  document.getElementById("chuVi").innerHTML = chuVis.toLocaleString();
}
// start lab 5
/*
- Input:
    + Số có 2 chữ số
- Process:
    + Tạo biến số có 2 chữ số
    + Tạo biến hàng đơn vị, hàng chục
    + Gán giá trị cho biến số có 2 chữ số
    + Tính tổng của 2 ký số = hàng đơn vị + hàng chục
    + In ra màn hình tổng của 2 ký số
- Output:
    + Tổng của 2 ký số
 */
function tong2KySo() {
  var number = parseInt(document.getElementById("kySo").value);

  var soHangDV = number % 10;
  var soHangChuc = Math.floor(number / 10);

  var tongKySos = 0;
  tongKySos = soHangDV + soHangChuc;

  document.getElementById("tongKySo").innerHTML = tongKySos.toLocaleString();
}
